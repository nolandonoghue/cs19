Note the  <INSERT YOUR LOCAL PATH TO THIS GIT REPO FOLDER> should be replaced with an actual path. You should follow my instructions for how to get a path that's applicable to you and replace it. **You'll be replacing both the angle brackets ( < and >) as well as everything between them**. If you're unsure if you understand, ask me.


docker run -p 8888:8888 -v <INSERT YOUR LOCAL PATH TO THIS GIT REPO FOLDER>:/home/jovyan jupyter/minimal-notebook